# Fuzzing

## Setup

* `rustup override set nightly`
* `cargo install cargo-fuzz`

see also https://www.wzdftpd.net/blog/rust-fuzzers.html

## Run

* `./scripts/fuzz`
