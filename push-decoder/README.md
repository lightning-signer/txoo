# Bitcoin Push Decoder

This crate provides a push style decoder for Bitcoin transactions and blocks.

## Motivation

In limited memory environments, a block might not fit in memory.  This crate provides a push style decoder that can be used to decode a block or transaction in a streaming fashion.
